import pymongo
import pandas as pd
from datetime import datetime
import re
import numpy as np
import matplotlib.pyplot as plt

def to_yyyymmdd(x):
    # print(x)
    x = str(x)
    x = x.split("/")
    if len(x) == 3:
        if  "." in x[1] or "." in x[2]:
            x[1] = x[1].replace(".","")
            x[2] = x[2].replace(".","")
        if  x[1]== "**" or x[2]== "**" :
            x[1] = "06"
            x[2] = "01"
        x[0] =  ''.join([x for x in x[0] if x.isdigit()])
        x[1] =  ''.join([x for x in x[1] if x.isdigit()])
        x[2] =  ''.join([x for x in x[2] if x.isdigit()])
        if int(x[1]) == 2 and  int(x[2] )> 28:
            x[1] = "02"
            x[2] = "28"
        if  int(x[1]) >12 or int(x[1])< 1:
            x[1] = "06"
        if  int(x[2] )> 30 or int(x[2])< 1:
            x[2] = "01"
        

        x = str(abs(int(x[0]))+1911) + "/" + str(abs(int(x[1])))+ "/" + str(abs(int(x[2])))
        aa = datetime.strptime(x , '%Y/%m/%d').strftime("%Y/%m/%d")
    else:
        aa = "NaN"
    return str(aa)

myclient = pymongo.MongoClient("mongodb://localhost:27017")
mydb = myclient["wistron"]
mycol = mydb["buildin2"]
cursor = mycol.find({"使照號碼": { "$ne": "" }})
stock = pd.DataFrame(list(cursor)) #轉換成DataFrame
del stock['_id']

list1 = []
list2 = []
for i in range(len(stock.index)):
    try:
        date = stock.loc[i,"發照日期"]
        if stock.loc[i,"發照日期"]=="NaN" or not("/" in str(stock.loc[i,"發照日期"])):
            stri = stock.loc[i,"使照號碼"].split("使")[0]
            num =  ''.join([x for x in stri if x.isdigit()])
            date =num + "/01/01"
            # if "洪" in stri:
            #     stock.loc[i,"發照日期"] =str(stri[:len(stri)-2]) + "/01/01"
            # else:
            #     stock.loc[i,"發照日期"] =str(stri[:len(stri)-1]) + "/01/01"
        else:
            dates = stock.loc[i,"發照日期"].split("/")
            tri = stock.loc[i,"使照號碼"].split("使")[0]
            num =  ''.join([x for x in stri if x.isdigit()])
            date =num + "/" +dates[1] + "/" + dates[2]
        # stock.loc[i,"發照日期"] = to_yyyymmdd(stock.loc[i,"發照日期"])
        list2.append(date)
        list1.append(to_yyyymmdd(date))
    except:
        list2.append("111/01/01")
        list1.append("2023/01/01")

stock_new = stock.copy()
stock_new["發照日期_西元"] = list1
stock_new["發照日期_NEW"] = list2

stock_new = stock_new[stock_new["發照日期_西元"] != "2023/01/01"].reset_index(drop = True)

stock_new["鄉鎮市區"] = stock_new["鄉鎮市區"].str.replace('新北市', '', regex=False)
stock_new["鄉鎮市區"]

job_group = stock_new.groupby(['鄉鎮市區'])["使照號碼"].count()
# 發照數量最多('三重區')
print(job_group.idxmax())
# 發照數量最少('烏來區')
job_group.idxmin()

stock_new['Date'] = pd.to_datetime(stock_new['發照日期_西元'])
time_start = '2020/11/1'
time1 = pd.to_datetime(time_start)
stock_new['Dateaaa'] = abs(stock_new['Date']-time1)

# 最長發照期間
print(stock_new.loc[stock_new['Dateaaa'].idxmax(),["鄉鎮市區"]])
findpeple = stock_new.loc[stock_new['Dateaaa'].idxmax(),["起造人","設計人"]]
# 最長發照期間(起造人)
print(findpeple["起造人"])
# 最長發照期間(設計人)
print(findpeple["設計人"])
# 建物列表(起造人)
findpeplebup = stock_new[stock_new["起造人"] == findpeple["起造人"]].reset_index(drop = True)
# 建物列表(設計人)
findpepledes = stock_new[stock_new["設計人"] == findpeple["設計人"]].reset_index(drop = True)

stock_new["buildyear"] = datetime.now().year - stock_new['Date'].dt.year 
stock_new["分布"] = (stock_new["buildyear"] // 10 +1)*10
job_group_1 = stock_new.groupby(['鄉鎮市區',"分布"])["鄉鎮市區"].count()
# 輸出建物屋齡分布(三重區)
job_group_1.to_csv("E:\Wistron\out.csv",encoding="big5")