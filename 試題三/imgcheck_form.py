import requests
import os
import zipfile
import time
import pandas as pd
import csv 
from io import BytesIO
from PIL import Image
from sys import version_info
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.wait import WebDriverWait
import time
import base64
import json 
import urllib.request
import pytesseract
import ddddocr

browser = webdriver.Chrome(r"E:\Wistron\chromedriver.exe")
browser.get('http://www.goldenjade.com.tw/captchaCheck/check/imgcheck_form.html')
time.sleep(2)
if WebDriverWait(browser,5).until(lambda the_browser:the_browser.find_element("id","rand-img"),"查詢不到該元素"):
        img_base64 = browser.execute_script("""
        var ele = arguments[0];
        var cnv = document.createElement('canvas');
        cnv.width = ele.width; cnv.height = ele.height;
        cnv.getContext('2d').drawImage(ele, 0, 0);
        return cnv.toDataURL('image/jpeg').substring(22);    
        """, browser.find_element("xpath","//img[@id='rand-img']"))
        with open("E:\Wistron\Picture\captcha-2.png", 'wb') as image:
            image.write(base64.b64decode(img_base64))
else:
        print("找不到驗證碼元素")
ocr = ddddocr.DdddOcr()
with open('E:/Wistron/Picture/captcha-2.png', 'rb') as f:
    img_bytes = f.read()
res = ocr.classification(img_bytes)
browser.find_element("id", "checknum").send_keys(res)
browser.find_element("xpath", '//input[@value="送出"]').click()
