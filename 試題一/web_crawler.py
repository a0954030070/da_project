import requests
import os
import zipfile
import time
import pandas as pd
import csv 
from io import BytesIO
from PIL import Image
from sys import version_info
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.wait import WebDriverWait
import time
import base64
import json 
import urllib.request
import pytesseract
import ddddocr
import pymongo
from bs4 import BeautifulSoup

# 替換字元
def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

# 輸入查詢介面
def insertpagedata(TownshipsSID,roadname):
    # 指定查詢網址
    browser.get('https://building-management.publicwork.ntpc.gov.tw/bm_query.jsp?rt=3')
    time.sleep(2)
    # 關閉Alarm資訊
    if WebDriverWait(browser,5).until(lambda the_browser:the_browser.find_element("xpath", '//input[@onclick="clsDivMsg()" and @value="關閉"]'),"查詢不到該元素"):
        js = 'document.querySelectorAll("#div_bm_msg ")[0].style.display="block";'
        browser.execute_script(js)
        browser.find_element("xpath", '//input[@onclick="clsDivMsg()" and @value="關閉"]').click()
    # 取得驗證碼圖片
    if WebDriverWait(browser,5).until(lambda the_browser:the_browser.find_element("id","codeimg"),"查詢不到該元素"):
        img_base64 = browser.execute_script("""
        var ele = arguments[0];
        var cnv = document.createElement('canvas');
        cnv.width = ele.width; cnv.height = ele.height;
        cnv.getContext('2d').drawImage(ele, 0, 0);
        return cnv.toDataURL('image/jpeg').substring(22);
        """, browser.find_element("xpath","//img[@id='codeimg']"))
        # 儲存驗證碼圖片
        with open("E:\Wistron\Picture\captcha.png", 'wb') as image:
            image.write(base64.b64decode(img_base64))
    else:
        print("找不到驗證碼元素")
    # 識別驗證碼圖片
    ocr = ddddocr.DdddOcr()
    with open('E:/Wistron/Picture/captcha.png', 'rb') as f:
        img_bytes = f.read()
    # 取得驗證碼
    res = ocr.classification(img_bytes)
    # 輸入必要欄位
    Townships = TownshipsSID
    D1val = postdata[postdata['鄉鎮市區'] == Townships[3:]].reset_index(drop=True)
    browser.execute_script("document.getElementById('D1').value='"+str(D1val.loc[0,"區號"])+"'")
    # browser.find_element("id","D1").send_keys()
    browser.find_element("id", "D3").send_keys(roadname)
    browser.find_element("id", "Z1").send_keys(res)
    browser.find_element("class name", 'bouton-contact').click() 
    print(res)
# 擷取資料
def getdata(ll, ft):
    # 完成頁面到結束頁面
    for k in range(1,int(ft)+1):
        # 掠過完成頁面以前的頁面
        if k<=ll:
            # 當前頁面
            fulltext = browser.find_element("xpath", "/html/body/div/div/div[2]/section[2]/table/tfoot/tr/td/b").text
            fulltext = fulltext.split("\u3000")[3]
            d = {" ": "", "[": "", "第": "", "頁": "", "]":"", "共":""}
            fulltext = replace_all(fulltext, d)
            fulltextlist = fulltext.split("，")
            if int(fulltextlist[0]) != int(fulltextlist[1]):
                # 模擬滾輪滑動
                scroll = 300
                js = "document.documentElement.scrollTop='%s'" % scroll
                browser.execute_script(js)
                # 跳轉下頁
                browser.find_element("xpath", '/html/body/div/div/div[2]/section[2]/table/tfoot/tr/td/b/a[3]').click()
            time.sleep(5)
            continue
        try:
            # 抓取頁面
            if WebDriverWait(browser, 20).until(lambda the_browser: the_browser.find_element("class name", "responstable"), "查詢不到該元素"):
                # 抓取表格
                table = browser.find_element("class name", "responstable").get_attribute('outerHTML')
                # 解析html
                tables = pd.read_html(str(table), header=0)
                tables = tables[0][:len(tables[0])-1]
                print(tables)
                insert = []
                # 輸入至MongoDB
                for x in range(len(tables)):
                    insert.append({"使照號碼": tables["使照號碼"][x], "建照號碼": tables["建照號碼"][x]\
                    , "起造人": tables["起造人"][x], "設計人": tables["設計人"][x]\
                    , "建築地址(代表號)": tables["建築地址(代表號)"][x], "發照日期": tables["發照日期"][x]\
                    , "鄉鎮市區": SID  , "路": data.loc[i,"road"]\
                        })
                xx = mycol.insert_many(insert)
                # 跳轉下頁
                fulltext = browser.find_element("xpath","/html/body/div/div/div[2]/section[2]/table/tfoot/tr/td/b").text
                fulltext = fulltext.split("\u3000")[3]
                d = { " ": "", "[": "", "第": "", "頁": "", "]": "", "共": ""}

                fulltext = replace_all(fulltext, d)
                fulltextlist = fulltext.split("，")
                if int(fulltextlist[0]) != int(fulltextlist[1]):
                    scroll = 300
                    js = "document.documentElement.scrollTop='%s'" % scroll
                    browser.execute_script(js)
                    browser.find_element("xpath", '/html/body/div/div/div[2]/section[2]/table/tfoot/tr/td/b/a[3]').click()
        except:
            return k-1
        time.sleep(10)   
    return k

# 開啟mongoDB
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["wistron"]
mycol = mydb["building"]

# 取得區號
postdata = pd.read_excel("https://www.post.gov.tw/post/download/Zip32_11106.xls")
postdata["郵遞區號"] = postdata["郵遞區號"].astype(int).astype('str')
postdata["區號"] = postdata["郵遞區號"].str[:3]
postdata = postdata[["區號", '縣市名稱', '鄉鎮市區']].drop_duplicates()
# 取得路名
data = pd.read_csv("https://data.moi.gov.tw/MoiOD/System/DownloadFile.aspx?DATA=99E4BBC4-11E2-410C-B749-C604267DA1EE")
# 移除Big5無法識別的字元
data = data[(data["city"] == "新北市") & (~data["road"].str.contains(r"𢊬|皷|𡍼|舘|菓|焿|温|灔|㒬|峯|啓|厦|梘|\U000fffa8|\U000fff99|躭|横|邨|脚|竪|𫙮|鈎|梘|坂|𥕢|𩻸|猪|双|犂|窠"))].reset_index(drop=True)
# 取得已完成路名
finshdata1 = pd.read_csv("E:/Wistron/finish.csv",encoding='big5')
finshdata  = finshdata1.copy()
# 取得已完成區號+路名
# 路名可能重複
finshdata["check"] = finshdata["site_id"]+finshdata["road"]
# 開始爬蟲
browser = webdriver.Chrome(r"E:\Wistron\chromedriver.exe")
for i,SID in enumerate(data["site_id"]):
    # 掠過已完成路名
    aa = SID + data.loc[i,"road"]
    if aa in finshdata["check"].to_list():
        continue
    # 每10筆休息30 Sec
    if i %10 ==0 and i != 0:
        time.sleep(30)
    # 輸入查詢介面
    insertpagedata(SID,data.loc[i,"road"])
    # 避免抓取時網頁異常跳轉
    for z in range(5):
        try:
            # 檢查表格
            if WebDriverWait(browser, 20).until(lambda the_browser: the_browser.find_element("class name", "responstable"), "查詢不到該元素"):
                # 取得頁碼
                fulltext = browser.find_element("xpath", "/html/body/div/div/div[2]/section[2]/table/tfoot/tr/td/b").text
                
                if len(fulltext.split("\u3000")) > 3:
                    # 移除不必要的字元
                    fulltext = fulltext.split("\u3000")[3]
                    d = { " ": "", "[": "", "第": "", "頁": "", "]": "", "共": ""}
                    fulltext = replace_all(fulltext, d)
                    # 切割字元
                    fulltext = fulltext.split("，")
                    # 紀錄完成頁數
                    bepage = 0 
                    while True:
                        # 擷取資料
                        fp = getdata(bepage, fulltext[1])
                        if fp == int(fulltext[1]): 
                            break
                        else:
                            bepage = fp
                            time.sleep(60)
                            insertpagedata(SID,data.loc[i,"road"])
                break
        except:
            time.sleep(10)
            insertpagedata(SID,data.loc[i,"road"])

    finshdata1 = finshdata1.append(pd.Series({"city":data.loc[i,"city"], "site_id":data.loc[i,"site_id"], \
                "road":data.loc[i,"road"]}), ignore_index=True)
    finshdata1.to_csv("E:/Wistron/finish.csv",index=False,encoding='big5')
    time.sleep(10) 
browser.quit()